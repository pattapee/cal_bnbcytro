import { Request, Response, Router } from "express";
import { HTTPSTATUS_OK } from "../constants/HttpStatus";
import profixPriceServices from "../services/ProfixPriceServices";

const routes = Router();

// Default API
routes.get("/", async (req: Request, res: Response) => {
  res.status(HTTPSTATUS_OK).send({ api: "OK" });
});

// Services ProfixPrice
routes.get("/profixprice", profixPriceServices.GetProfixPrice);
routes.post("/profixprice/update", profixPriceServices.UpdateProfixPrice);

export default routes;
