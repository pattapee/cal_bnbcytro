import { Request, Response } from "express";
import { readFileSync, writeFileSync } from "fs";
import Joi from "joi";
import { Password } from "../constants/Authentication";
import {
  HTTPSTATUS_BADREQUEST,
  HTTPSTATUS_CONFLICT,
  HTTPSTATUS_CREATE,
  HTTPSTATUS_NOTFOUND,
  HTTPSTATUS_OK,
  HTTPSTAUS_FORBIDDEN,
} from "../constants/HttpStatus";

export default class ProfixPriceServices {
  public static UpdateProfixPrice = async (req: Request, res: Response) => {
    try {
      const schema = Joi.object().keys({
        password: Joi.string().required(),
        profixprice1: Joi.number().required(),
        profixprice2: Joi.number().required(),
        profixprice3: Joi.number().required(),
      });

      const value = await schema
        .validateAsync(req.body.data)
        .catch((err: { details: Array<{ message: string }> }) => {
          res.status(HTTPSTATUS_CONFLICT).send({
            data: "Failed to validate input " + err.details[0].message,
          });
        });

      if (value.password !== Password) {
        res.status(HTTPSTAUS_FORBIDDEN).send({ data: "Please verify user" });
      } else {
        const jsonProfix = JSON.stringify({
          profixprice1: value.profixprice1,
          profixprice2: value.profixprice2,
          profixprice3: value.profixprice3,
        });

        await writeFileSync("./profixprice.txt", String(jsonProfix), "utf8");

        const profixprice = await readFileSync("./profixprice.txt", "utf8");

        if (profixprice) {
          res.status(HTTPSTATUS_CREATE).send({ profixprice });
        }
      }
    } catch (e) {
      console.error(e);
      res.status(HTTPSTATUS_BADREQUEST).send({ data: e });
    }
  }

  public static GetProfixPrice = async (req: Request, res: Response) => {
    try {
      const profixprice = await readFileSync("./profixprice.txt", "utf8");

      if (profixprice) {
        res.status(HTTPSTATUS_OK).send(JSON.parse(profixprice));
      } else {
        res.status(HTTPSTATUS_NOTFOUND).send({ data: "Invalid ProfixPrice" });
      }
    } catch (e) {
      console.error(e);
      res.status(HTTPSTATUS_BADREQUEST).send({ data: e });
    }
  }
}
