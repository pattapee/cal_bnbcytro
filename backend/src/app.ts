import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import * as express from "express";
import helmet from "helmet";
import routes from "./api";

dotenv.config();

const port = process.env.SERVER_PORT;
const app = express.default();

app.use(cors());
app.use(helmet());
app.use(bodyParser.json()).use(bodyParser.urlencoded({ extended: true }));

app.use("/", routes);

app.listen(port, async () => {
  console.log(`Server started on port ${port}`);
});
