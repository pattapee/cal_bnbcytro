import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Calculator from "./Page/Calculator";
import CalculatorGuest from "./Page/CalculatorGuest";
import Management from "./Page/Management";
import Navbar from "./Component/Navbar";
import NavbarGuest from "./Component/NavbarGuest";
import Footer from "./Component/Footer";

const AppWithRouter = () => (
  <BrowserRouter>
    <Routes>
      <Route
        path="/"
        element={
          <>
            <NavbarGuest />
            <CalculatorGuest />
            <Footer />
          </>
        }
      />
      <Route
        path="/calculator"
        element={
          <>
            <Navbar />
            <Calculator />
            <Footer />
          </>
        }
      />
      <Route
        path="/management"
        element={
          <>
            <Navbar />
            <Management />
            <Footer />
          </>
        }
      />
    </Routes>
  </BrowserRouter>
);

ReactDOM.render(<AppWithRouter />, document.getElementById("root"));
reportWebVitals();
