import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import Swal from "sweetalert2";

function Management(props) {
  const [profix, setProfix] = useState({});
  const { register, handleSubmit, reset, setValue } = useForm();

  const onSubmit = async (data) => {
    console.log(data);
    try {
      const result = await axios.post(
        `${process.env.REACT_APP_API_URL}/profixprice/update`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          data,
        }
      );

      if (result.data) {
        const { profixprice1, profixprice2, profixprice3 } = JSON.parse(
          result.data.profixprice
        );

        setValue("profixprice1", profixprice1);
        setValue("profixprice2", profixprice2);
        setValue("profixprice3", profixprice3);
        reset({ password: "" });

        Swal.fire(
          "แก้ไขข้อมูลสำเร็จ !",
          `กำไรเงื่อนไขที่ 1: ${data.profixprice1} <br>
          กำไรเงื่อนไขที่ 2: ${data.profixprice2} <br>
          กำไรเงื่อนไขที่ 3: ${data.profixprice3}`,
          "success"
        );
      }
    } catch (err) {
      Swal.fire("ไม่สามารถแก้ไขข้อมูลได้ !", `${err}`, "error");
    }
  };

  const fnGetProfixPrice = async () => {
    const result = await axios.get(
      `${process.env.REACT_APP_API_URL}/profixprice`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    setProfix(result.data);
    setValue("profixprice1", result.data.profixprice1);
    setValue("profixprice2", result.data.profixprice2);
    setValue("profixprice3", result.data.profixprice3);
  };

  useEffect(() => {
    async function fetchAPI() {
      await fnGetProfixPrice();
    }

    fetchAPI();
  }, []);

  return (
    <>
      <header className="bg-white shadow">
        <div className="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl text-center font-bold underline">
            {"Management"}
          </h1>
          <section className="hero container max-w-screen-lg mx-auto pb-10 flex">
            <img
              className="mx-auto h-52 w-44"
              src="../image/logo-zeus-gas.jpg"
              alt="screenshot"
            />
          </section>
        </div>
      </header>
      <main>
        <div className="max-w-6xl mx-auto py-6 sm:px-6 lg:px-8">
          <div className="px-4 py-6 sm:px-0">
            <form
              className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
              onSubmit={handleSubmit(onSubmit)}
            >
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  บวกกำไรเพิ่มในเงื่อนไขที่ [0.0x - 0.2x]
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="profixprice1"
                  type="number"
                  placeholder="ระบุกำไรเพิ่ม"
                  required
                  {...register("profixprice1")}
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  บวกกำไรเพิ่มในเงื่อนไขที่ [0.3 - 0.8]
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="profixprice2"
                  type="number"
                  placeholder="ระบุกำไรเพิ่ม"
                  required
                  {...register("profixprice2")}
                />
              </div>
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  บวกกำไรเพิ่มในเงื่อนไขที่ [0.81+]
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="profixprice3"
                  type="number"
                  placeholder="ระบุกำไรเพิ่ม"
                  required
                  {...register("profixprice3")}
                />
              </div>
              <div className="mb-6">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Token
                </label>
                <input
                  className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  type="text"
                  placeholder="ระบุ token"
                  required
                  {...register("password")}
                />
              </div>
              <div className="flex items-center justify-center">
                <button
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="submit"
                >
                  ยืนยันข้อมูล
                </button>
              </div>
            </form>
          </div>
        </div>
      </main>
    </>
  );
}

export default Management;
