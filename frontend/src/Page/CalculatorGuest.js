import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Swal from "sweetalert2";

function CalculatorGuest() {
  const [profix, setProfix] = useState({});
  const [BNB, setBNB] = useState();
  const [reqMoney, setReqMoney] = useState(0);
  const [reqBNB, setReqBNB] = useState(0);
  const [priceRangeBNB, setPriceRangeBNB] = useState(0);
  const [nettotal, setNetTotal] = useState({});
  const reqBNBRef = useRef(null);

  useEffect(() => {
    async function fetchAPI() {
      await fnInitial();
    }
    reqBNBRef.current.focus();
    fetchAPI();
  }, [BNB]);

  const fnGetProfixPrice = async () => {
    const profix = await axios.get(
      `${process.env.REACT_APP_API_URL}/profixprice`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    setProfix(profix.data);
  };

  const fnInitial = async () => {
    await fnGetProfixPrice();
    await fnGetBNBPrice();
  };

  const fnGetBNBPrice = async () => {
    const BNB = await axios.get(
      `https://api.coingecko.com/api/v3/simple/price?ids=binancecoin&vs_currencies=THB`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    setBNB(BNB.data.binancecoin.thb);
    convertPriceBNB(BNB.data.binancecoin.thb);
  };

  const convertPriceBNB = async (newBNB) => {
    const num0 = String(newBNB).charAt(0);
    const num1 = String(newBNB).charAt(1);
    let num2 = String(newBNB).charAt(2);

    if (num2 < 3) {
      num2 = 0;
      setPriceRangeBNB(Number(`${num0}${num1}${num2}00`));
    } else if (num2 <= 7 && num2 >= 3) {
      num2 = 5;
      setPriceRangeBNB(Number(`${num0}${num1}${num2}00`));
    } else if (num2 >= 8) {
      num2 = 0;
      setPriceRangeBNB(Number(`${Number(`${num0}${num1}${num2}`) + 10}00`));
    }
  };

  const handleCalulator = async () => {
    if (reqBNB && reqBNB > 1) {
      Swal.fire(
        "ไม่สามารถคำนวนข้อมูลได้ !",
        `BNB มีค่ามากกว่าที่กำหนด`,
        "error"
      );
      await clearReqData();

      return;
    } else if (reqMoney && reqMoney < 100) {
      Swal.fire(
        "ไม่สามารถคำนวนข้อมูลได้ !",
        `จำนวนเงินที่ใส่มีค่าน้อยกว่า 100`,
        "error"
      );
      await clearReqData();

      return;
    }

    await fnInitial();
    if (reqMoney === 0) {
      await calResult1(reqBNB);
    } else if (reqBNB === 0) {
      await calResult2();
    } else if (reqBNB !== 0 && reqMoney !== 0) {
      Swal.fire("ไม่สามารถคำนวนข้อมูลได้ !", `โปรดกดปุ่มล้างข้อมูล`, "info");
    }
  };

  const calResult1 = async (newReqBNB) => {
    const integerProfix = await calIntegerProfix();
    const calTotal = Number((priceRangeBNB + integerProfix) * newReqBNB);
    const calCost = Number(priceRangeBNB * newReqBNB);

    const totalProfix = calTotal - calCost;
    const result = {
      reqBNB: Number(newReqBNB),
      reqMoney: Math.round(calTotal),
      calTotal,
      calCost,
      priceRangeBNB,
      integerProfix,
      totalProfix,
    };
    setReqBNB(Number(newReqBNB).toFixed(4));
    setReqMoney(Math.round(calTotal));
    setNetTotal(result);
  };

  const calResultTest2 = async (newReqBNB) => {
    const integerProfix = await calIntegerProfix(newReqBNB);
    const calTotal = Number((priceRangeBNB + integerProfix) * newReqBNB);
    const calCost = Number(priceRangeBNB * newReqBNB);

    const totalProfix = calTotal - calCost;
    const result = {
      reqBNB: Number(newReqBNB),
      reqMoney: Math.round(calTotal),
      calTotal,
      calCost,
      priceRangeBNB,
      integerProfix,
      totalProfix,
    };
    return result;
  };

  const calResult2 = async () => {
    let j = 0;
    let ans = true;
    let result = {};
    while (ans === true) {
      result = await calResultTest2(j);
      j += 0.0001;

      if (
        result.reqMoney >= Number(reqMoney) - 3 &&
        result.reqMoney <= Number(reqMoney) + 3
      ) {
        ans = false;
      }
    }
    setReqBNB(Number(result.reqBNB).toFixed(4));
    setReqMoney(Math.round(result.reqMoney));
    setNetTotal(result);
  };

  const calIntegerProfix = async (value) => {
    const valueBNB = reqBNB || value;

    if (valueBNB < 0.3) {
      return profix.profixprice1 * 100;
    } else if (valueBNB >= 0.3 && valueBNB < 0.81) {
      return profix.profixprice2 * 100;
    } else if (valueBNB >= 0.81) {
      return profix.profixprice3;
    }
  };

  const handleOnChangeBNB = async (value) => {
    setReqBNB(value);
  };

  const handleOnChangeMoney = async (value) => {
    setReqMoney(value);
  };

  const clearReqData = async () => {
    setReqBNB(Number(0));
    setReqMoney(Number(0));
    setNetTotal({});
    reqBNBRef.current.focus();
    await fnInitial();
  };

  return (
    <>
      <header className="bg-white shadow">
        <div className="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl text-center font-bold underline">
            {"ZEUS GAS"}
          </h1>
          <section className="hero container max-w-screen-lg mx-auto pb-10 flex">
            <img
              className="mx-auto h-52 w-44"
              src="../image/logo-zeus-gas.jpg"
              alt="screenshot"
            />
          </section>
        </div>
      </header>
      <main>
        <div className="max-w-6xl mx-auto py-6 sm:px-6 lg:px-8">
          <div className="px-4 py-6 sm:px-0">
            <form className="bg-white shadow-md rounded px-8 pt-2 pb-8 mb-4">
              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  1 BNB = {BNB} บาท
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 bg-gray-300 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="reqBNB"
                  type="number"
                  value={priceRangeBNB}
                  readOnly
                />
              </div>

              <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  จำนวน BNB ที่ต้องการเติม
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="reqBNB"
                  type="number"
                  placeholder="จำนวน BNB ที่ต้องการเติม"
                  value={reqBNB}
                  max={1}
                  onChange={(e) => {
                    handleOnChangeBNB(e.target.value);
                  }}
                  ref={reqBNBRef}
                />
              </div>
              <div className="mb-6">
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  จำนวนเงินที่ต้องชำระ
                </label>
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="reqmoney"
                  type="number"
                  min={0}
                  max={10000000}
                  placeholder="จำนวนเงินที่ต้องชำระ"
                  min={100}
                  value={reqMoney}
                  onChange={(e) => {
                    handleOnChangeMoney(e.target.value);
                  }}
                />
              </div>
              <div className="flex items-center justify-center">
                <button
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="button"
                  onClick={handleCalulator}
                >
                  ยืนยันการคำนวน
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button
                  className="bg-gray-400 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="button"
                  onClick={clearReqData}
                >
                  ล้างค่า
                </button>
              </div>
            </form>
          </div>
        </div>
      </main>
    </>
  );
}

export default CalculatorGuest;
