function Footer() {
  return (
    <>
      <footer className="text-center bg-yellow-400 text-white ">
        <div className="px-6 pt-2">
          <div className="flex justify-center mb-6">
            {
              <div className="text-center p-3">
                © 2021 Copyright <br />
                Calculator Binance Coin
              </div>
            }
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
